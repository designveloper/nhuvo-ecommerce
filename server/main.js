import { Meteor } from "meteor/meteor";
import { createProductData } from "./migration/product";
import { createProductDetail } from "./migration/productDetail";
import { createCategory } from "./migration/category";
import { CreateCart } from "./migration/cart";
import { createOrder } from "./migration/order";
// import { createUser } from "./migration/user";
import "../imports/api/authentication";
import "../imports/api/displayProduct";
import "../imports/api/productDetails";
import "../imports/api/categories";
import "../imports/api/carts";
import "../imports/api/addToCart";
import "../imports/api/addOrder"

Meteor.startup(() => {
  //   // code to run on server at startup
  if (Meteor.isServer) {
    Migrations.add({
      version: 1,
      up: function() {
        // Product Migration
        try {
          createProductData();
          createProductDetail();
          createCategory();
          CreateCart();
          createOrder();
          // createUser();
        } catch (error) {
          if (error) {
            console.log(error);
          }
        }
      }
    });

    // Run all migration
    Meteor.startup(() => {
      Migrations.migrateTo("1");
    });
  }
});
