//import data here

import productDetail from "./productDetail";
import { ProductDetails } from "../../../imports/api/productDetails";

export default (createProductDetail = () => {
  productDetail.forEach(product => {
    ProductDetails.insert(product, (err, data) => {
      if (err) {
        console.log(err);
        return;
      } else {
        console.log("productDetail is inserted");
      }
    });
  });
});
