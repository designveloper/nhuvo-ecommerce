import productData from "./productData"; //import data here
import { Products } from "../../../imports/api/products";

export default (createProductData = () => {
  productData.forEach(product => {
    Products.insert(product, (err, data) => {
      if (err) {
        console.log(err);
        return;
      }
    });
  });
});
