import createProductData from "./product";
import createUser from "./user";

export { createProductData, createUser };
