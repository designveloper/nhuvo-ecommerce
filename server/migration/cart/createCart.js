import { Carts } from "../../../imports/api/carts";
import cartData from "./cartData";

export default (createCart = () => {
  cartData.forEach(product => {
    Carts.insert(product, (err, data) => {
      if (err) {
        console.log(err);
        return;
      }
    });
  });
});
