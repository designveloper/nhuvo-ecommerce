import React from "react";
import { Meteor } from "meteor/meteor";
import { render } from "react-dom";

import Routes from "../lib/Routes.js";

Meteor.startup(() => {
  render(<Routes />, document.getElementById("render-target"));
});
