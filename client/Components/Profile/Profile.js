import React, { Component } from "react";
import Header from "../../partial/Header";
import Footer from "../../partial/Footer";
import "../../../public/views/Profile.css";

class Productlist extends Component {
  constructor() {
    super();
    this.state = {
      accSet: false,
      passSet: false,
      edit: false,
      accSetLeft: true,
      accSetPass: false
    };
  }
  render() {
    return (
      <div>
        <Header />
        <div className="profile">
          <div className="account">
            <p className="my-account">My Account</p>
            <p className="information">
              {this.state.accSetLeft ? "Information" : "Change password"}
            </p>
            {this.state.edit || this.state.accSetPass ? null : (
              <p
                className="edit-info"
                onClick={() => this.setState({ edit: true })}
                style={{ cursor: "pointer" }}
              >
                Edit
              </p>
            )}
          </div>

          {this.state.accSetLeft ? (
            <div
              className="acc-setting"
              style={{ color: "var(--bright-orange)" }}
              onClick={() => {
                this.setState({
                  accSetLeft: true,
                  accSetPass: false,
                  edit: false
                });
              }}
            >
              Account setting
            </div>
          ) : (
            <div
              className="acc-setting"
              style={{ color: " var(--greyish-brown)" }}
              onClick={() => {
                this.setState({
                  accSetLeft: true,
                  accSetPass: false,
                  edit: false
                });
              }}
            >
              Account setting
            </div>
          )}
          {this.state.accSetPass ? (
            <div
              className="change-pass"
              style={{ color: "var(--bright-orange)" }}
              onClick={() => {
                this.setState({
                  accSetLeft: false,
                  accSetPass: true,
                  edit: false
                });
              }}
            >
              Change password
            </div>
          ) : (
            <div
              className="change-pass"
              style={{ color: "var(--greyish-brown)" }}
              onClick={() => {
                this.setState({
                  accSetLeft: false,
                  accSetPass: true,
                  edit: false
                });
              }}
            >
              Change password
            </div>
          )}
          {this.state.edit ? (
            <div className="change-info">
              <div className="tittle2">
                <p style={{ marginTop: "26px", marginBottom: "0%" }}>NAME</p>
              </div>
              <div>
                <input
                  type="text"
                  placeholder="Enter your name..."
                  className="input-box"
                />
              </div>
              <p
                className="tittle2"
                style={{ marginTop: "33px", marginBottom: "0%" }}
              >
                E-MAIL
              </p>
              <input
                type="password"
                placeholder="Enter your e-mail..."
                className="input-box"
              />
              <br />
              <button
                type="button"
                onClick={() => {
                  this.setState({ edit: false });
                }}
                className="cancel-butt"
              >
                Cancel
              </button>
              <button
                type="button"
                onClick={() => {
                  this.setState({ edit: false });
                }}
                className="save-butt"
              >
                Save
              </button>
            </div>
          ) : this.state.accSetPass ? (
            <div className="edit-pass">
              <div className="tittle2">
                <p style={{ marginTop: "26px", marginBottom: "0%" }}>
                  CURRENT PASSWORD
                </p>
              </div>
              <div>
                <input
                  type="password"
                  placeholder="Enter your password..."
                  className="input-box"
                />
              </div>
              <div className="tittle2">
                <p style={{ marginTop: "26px", marginBottom: "0%" }}>
                  NEW PASSWORD
                </p>
              </div>

              <input
                type="password"
                placeholder="Enter your password..."
                className="input-box"
              />
              <div className="tittle2">
                <p style={{ marginTop: "26px", marginBottom: "0%" }}>
                  RE-ENTER NEW PASSWORD
                </p>
              </div>

              <input
                type="password"
                placeholder="Enter your password..."
                className="input-box"
              />
              <button
                type="button"
                onClick={() => {
                  this.setState({ edit: false });
                }}
                style={{ marginLeft: "310px" }}
                className="save-butt"
              >
                Save
              </button>
            </div>
          ) : (
            <div className="display-info">
              <p className="tittle" style={{ marginTop: "26px" }}>
                Name
              </p>
              <p className="user-info">Johnnie Kennedy</p>
              <p className="tittle" style={{ marginTop: "31px" }}>
                E-mail
              </p>
              <p className="user-info">Johnniekenedy@gmail.com</p>
            </div>
          )}
        </div>
        <Footer />
      </div>
    );
  }
}

export default Productlist;
