import React, { Component } from "react";
import "../../../public/views/Page404";

class Page404 extends Component {
  render() {
    return (
      <div className="site">
        <div className="sketch">
          <div className="bee-sketch red" />
          <div className="bee-sketch blue" />
        </div>

        <h1>
          404:
          <small>Page Not Found</small>
          <br />
          <a href="/" style={{ fontSize: "30px", color: "green" }}>
            Click here to go back
          </a>
        </h1>
      </div>
    );
  }
}

export default Page404;
