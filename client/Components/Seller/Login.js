import React, { Component } from "react";
import { check } from "meteor/check";
import { withTracker } from "meteor/react-meteor-data";
import { Meteor } from "meteor/meteor";
import { Route, Redirect } from "react-router-dom";
import "../../../public/views/Login";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loginEmail: "",
      loginPass: "",
      redirect: false,
      redirectAdmin: false
    };
  }

  handleChange = event => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };

  submitLogin = event => {
    event.preventDefault();
    const self = this;
    const { component: Component, ...rest } = this.props;
    const users = [
      {
        email: this.state.loginEmail,
        password: this.state.loginPass
      }
    ];

    check(users, [
      {
        email: String,
        password: String
      }
    ]);
    users.forEach(({ email, password }) => {
      Meteor.loginWithPassword(email, password, function(err) {
        if (err) {
          alert(err.reason);
        } else {
          // alert("login successfully!");
          console.log(Meteor.user().role);
          if (Meteor.user().role === "admin") {
            alert("welcome admin");
            self.setState({
              redirectAdmin: true
            });
          } else {
            alert("you're not admin");
            self.setState({
              redirect: true
            });
          }
        }
      });
    });
  };

  render() {
    if (this.state.redirect) return <Redirect to="/" />;
    if (this.state.redirectAdmin) return <Redirect to="/admin/order" />;
    return (
      <div className="admin-login">
        <div className="main-logo">
          <a href="/">
            <img src="/img/logo2.svg" />
          </a>
        </div>
        <div className="admin-form">
          <div className="admin-login-title">Log in</div>
          <div style={{ marginLeft: "32px" }}>
            <div className="admin-email">EMAIL</div>
            <form onSubmit={this.submitLogin}>
              <input
                type="text"
                placeholder="email@sample.com"
                className="admin-input"
                value={this.state.loginEmail}
                name="loginEmail"
                onChange={this.handleChange}
              />

              <div className="admin-email">PASSWORD</div>

              <input
                type="password"
                placeholder="Enter password"
                className="admin-input"
                value={this.state.loginPass}
                name="loginPass"
                onChange={this.handleChange}
              />
              <button type="submit" className="admin-login-button">
                Log in
              </button>
            </form>
          </div>
          <div className="admin-forgot">Forgot password</div>
        </div>
      </div>
    );
  }
}

export default withTracker(() => {
  Meteor.subscribe("usersAdmin");
  return {
    user: Meteor.user()
  };
})(Login);
