import React, { Component } from "react";
import { Meteor } from "meteor/meteor";
import { Redirect } from "react-router-dom";

class Order extends Component {
  constructor() {
    super();
    this.state = {
      redirect: false
    };
  }
  render() {
    if (this.state.redirect) {
      <Redirect to="/404page" />;
    }
    const self = this;
    return (
      <div>
        <div>I should display all orders</div>
      </div>
    );
  }
}

export default Order;
