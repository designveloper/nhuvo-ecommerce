import React, { Component } from "react";

import Footer from "../../partial/Footer";
import Header from "../../partial/Header";
import "/public/views/Homepage.css";
// import Menu from "../../partial/Menu";

class Homepage extends Component {
  constructor() {
    super();
    this.state = {};
  }
  render() {
    return (
      <div>
        <Header />
        <div className="home">
          <div className="cover">
            <img src="/img/1.jpg" className="cover-img" />
            <div className="outfit">OUTFIT OF THE WEEK</div>
            <button
              className="shopnow"
              type="button"
              style={{ top: "569px", right: "203px" }}
            >
              Shop now
            </button>
          </div>

          <div className="cover-collect">
            <div className="cover-pic">
              <div className="cover-div">
                <img src="/img/2.jpg" className="cover-small" />
                <div
                  className="discript"
                  style={{ top: "289px", right: "113px" }}
                >
                  Men
                </div>
                <hr className="cover-hr" />
                <button
                  className="shopnow"
                  type="button"
                  style={{ top: "341px", right: "70px" }}
                >
                  Shop now
                </button>
              </div>
              <div className="cover-div">
                <img src="/img/3.jpg" className="cover-small" />
                <div
                  className="discript"
                  style={{ top: "289px", right: "113px" }}
                >
                  Ladies
                </div>
                <hr className="cover-hr" />
                <button
                  className="shopnow"
                  type="button"
                  style={{ top: "341px", right: "70px" }}
                >
                  Shop now
                </button>
              </div>
              <div className="cover-div">
                <img src="/img/4.jpg" className="cover-small" />
                <div
                  className="discript"
                  style={{ top: "289px", right: "113px" }}
                >
                  Girls
                </div>
                <hr className="cover-hr" />
                <button
                  className="shopnow"
                  type="button"
                  style={{ top: "341px", right: "70px" }}
                >
                  Shop now
                </button>
              </div>
              <div className="cover-div">
                <img src="/img/5.jpg" className="cover-small" />
                <div
                  className="discript"
                  style={{ top: "289px", right: "113px" }}
                >
                  Boys
                </div>
                <hr className="cover-hr" />
                <button
                  className="shopnow"
                  type="button"
                  style={{ top: "341px", right: "70px" }}
                >
                  Shop now
                </button>
              </div>
            </div>
          </div>
        </div>

        <Footer />
      </div>
    );
  }
}

export default Homepage;
