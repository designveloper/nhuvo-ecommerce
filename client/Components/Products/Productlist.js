import React, { Component } from "react";
import Header from "../../partial/Header";
import Footer from "../../partial/Footer";
import ProductProfile from "./ProductProfile";
import { withTracker } from "meteor/react-meteor-data";
import { Meteor } from "meteor/meteor";
import "../../../public/views/Productlist.css";
import { Products } from "../../../imports/api/products";

class Productlist extends Component {
  static ProductProfile = ProductProfile;
  constructor(props) {
    super(props);
    this.state = {
      sortBy: "Popularity",
      sizeS: false,
      sizeM: false,
      sizeL: false,
      sizeLDisable: false,
      totalPage: 0,
      offset: 0,
      currentPage: 1,
      perPage: 20,
      allProduct: undefined
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick = param => event => {
    // this.setState({
    //   currentPage: Number(event.target.id)
    // });
    if (param === "forward") {
      if (
        this.state.currentPage <
        Math.ceil(this.props.products.length / this.state.perPage)
      ) {
        this.setState({
          currentPage: this.state.currentPage + 1
        });
      }
    }

    if (param === "back") {
      if (this.state.currentPage > 1) {
        this.setState({
          currentPage: this.state.currentPage - 1
        });
      }
    }
  };

  render() {
    const indexOfLast = this.state.currentPage * this.state.perPage;
    const indexOfFirst = indexOfLast - this.state.perPage;
    // let newlist;
    let currentList;
    currentList = [];
    if (this.props.products) {
      if (this.state.sortBy === "Name: A - Z") {
        this.state.allProduct = this.props.products.sort((a, b) =>
          a.name >= b.name ? 1 : -1
        );
        currentList = this.state.allProduct.slice(indexOfFirst, indexOfLast);
      }
      if (this.state.sortBy === "Price: lowest to highest") {
        this.state.allProduct = this.props.products.sort((a, b) =>
          a.price >= b.price ? 1 : -1
        );
        currentList = this.state.allProduct.slice(indexOfFirst, indexOfLast);
      }
      if (this.state.sortBy === "Price: highest to lowest") {
        this.state.allProduct = this.props.products.sort((a, b) =>
          a.price >= b.price ? -1 : 1
        );
        currentList = this.state.allProduct.slice(indexOfFirst, indexOfLast);
      }
      if (this.state.sortBy === "Popularity") {
        currentList = this.props.products.slice(indexOfFirst, indexOfLast);
      }
    }

    // console.log(currentList);

    const renderProduct = Object.keys(currentList).map((product, i) => {
      // return <li key={i}>{currentList[product].name}</li>;
      let url;
      url = "/product-list/" + currentList[product]._id.toString();
      return (
        <div className="polaroid" key={i}>
          <img src={currentList[product].image[0]} alt="5 Terre" />
          <div className="container">
            <p>
              <a href={url}>{currentList[product].name}</a>
            </p>
            <p
              style={{
                color: "var(--greyish-brown)",
                fontWeight: "normal"
              }}
            >
              ${currentList[product].price}
            </p>
          </div>
        </div>
      );
    });

    return (
      <div>
        <Header />
        <div className="ladies-dress">
          <div className="page-route">
            Ladies/ Dresses{this.props.match.params.category}
          </div>
        </div>
        <div className="product-header">
          <p className="category">Category</p>
          <div className="sort-by">
            <p style={{ width: "47px", height: "22px" }}>Sort By: </p>{" "}
            <p
              style={{
                fontWeight: "bold",
                lineHeight: "1.5",
                color: "var(--dark-grey)",
                paddingLeft: "3.5px"
              }}
            >
              {this.state.sortBy}
            </p>
            <img src="/img/arrow2.svg" style={{ margin: "0 0 0 12px" }} />
            <div id="sort-dropdown">
              <div className="sort-list">
                <div
                  onClick={() => this.setState({ sortBy: "Popularity" })}
                  // style={{ paddingTop: "3px" }}
                >
                  Popularity
                </div>{" "}
                <br />
                <hr />
                <div onClick={() => this.setState({ sortBy: "Name: A - Z" })}>
                  Name: A-Z
                </div>
                <br />
                <hr />
                <div
                  onClick={() =>
                    this.setState({ sortBy: "Price: lowest to highest" })
                  }
                >
                  Price: lowest to highest
                </div>{" "}
                <br />
                <hr />
                <div
                  onClick={() =>
                    this.setState({ sortBy: "Price: highest to lowest" })
                  }
                >
                  Price: highest to lowest
                </div>
              </div>
            </div>
          </div>
          <div className="pagination">
            <img
              src="/img/arrow.svg"
              style={{
                width: "24px",
                height: "24px",
                transform: "rotate(90deg)",
                cursor: "pointer"
              }}
              onClick={this.handleClick("back")}
            />
            <p className="paginate-number">
              {this.state.currentPage}/
              {Math.ceil(this.props.products.length / this.state.perPage)}
            </p>
            <img
              src="/img/arrow.svg"
              style={{
                width: "24px",
                height: "24px",
                transform: "rotate(270deg)",
                cursor: "pointer"
              }}
              onClick={this.handleClick("forward")}
            />
          </div>
        </div>

        <div className="list-body">
          <div className="filters">
            <div style={{ color: "var(--bright-orange)" }}>All dresses</div>
            <hr
              style={{
                width: "20px",
                height: "3px",
                color: "solid 0.5px #979797",
                margin: "8px 0 7px 0"
              }}
            />
            <div style={{ marginTop: "0" }}>Rompers/ Jumpsuits</div>
            <div>Casual dresses</div>
            <div>Going out dresses</div>
            <div>Party/ Ocassion dresses</div>
            <div>Mini dresses</div>
            <div>Maxi/ Midi dresses</div>
            <div>Sets</div>
            <hr style={{ margin: "54px 0 47.9px 0", width: "101px" }} />
            <div
              style={{
                fontSize: "16px",
                fontWeight: "bold",
                lineHeight: "1.5",
                color: "var(--dark-grey)",
                margin: "47.9px 0 33px 0"
              }}
            >
              Filter
            </div>
            <div className="filter-size">
              Size <img src="/img/arrow2.svg" style={{ float: "right" }} />
              <div id="size-drop">
                <hr
                  style={{
                    margin: "8px 0 14.9px 0",
                    width: "180px",
                    height: "0.1px",
                    border: "dashed 0.5px #cccccc"
                  }}
                />
                <div style={{ flexWrap: "nowrap", display: "flex" }}>
                  {this.state.sizeS ? (
                    <button
                      style={{
                        width: "40px",
                        height: "40px",
                        border: "none",
                        backgroundColor: "var(--pale-orange)",
                        color: "white"
                      }}
                      onClick={() => this.setState({ sizeS: false })}
                    >
                      S
                    </button>
                  ) : (
                    <button
                      onClick={() => this.setState({ sizeS: true })}
                      className="size-button"
                    >
                      S
                    </button>
                  )}
                  {this.state.sizeM ? (
                    <button
                      style={{
                        width: "40px",
                        height: "40px",
                        border: "none",
                        marginLeft: "16px",
                        backgroundColor: "var(--pale-orange)",
                        color: "white"
                      }}
                      onClick={() => this.setState({ sizeM: false })}
                    >
                      M
                    </button>
                  ) : (
                    <button
                      style={{
                        marginLeft: "16px"
                      }}
                      className="size-button"
                      onClick={() => this.setState({ sizeM: true })}
                    >
                      M
                    </button>
                  )}
                  <button
                    style={{
                      width: "40px",
                      height: "40px",
                      border: "solid 1px var(--white-four)",
                      marginLeft: "16px",
                      background: "transparent",
                      opacity: "0.6",
                      cursor: "not-allowed"
                    }}
                  >
                    L
                  </button>
                </div>
              </div>
            </div>

            <hr
              style={{
                width: "180px",
                height: "0.1px",
                border: "solid 0.5px #cccccc",
                margin: "9px 0 8.9px 0"
              }}
            />

            <div className="filter-color">
              Color <img src="/img/arrow2.svg" style={{ float: "right" }} />
              <div id="color-drop">
                <hr
                  style={{
                    margin: "8px 0 14.9px 0",
                    width: "180px",
                    height: "0.1px",
                    border: "dashed 0.5px #cccccc"
                  }}
                />
                <div
                  style={{
                    // flexWrap: "nowrap",
                    // display: "flex",
                    // wordWrap: "break-word"
                    lineHeight: "3"
                  }}
                >
                  <button
                    style={{
                      width: "30px",
                      height: "30px",
                      backgroundColor: "#ff5f6d",
                      border: "none",
                      borderRadius: "50%",
                      marginRight: "18px"
                    }}
                  />
                  <button
                    style={{
                      width: "30px",
                      height: "30px",
                      backgroundColor: "rgba(255, 213, 67, 0.4)",
                      border: "none",
                      borderRadius: "50%",
                      marginRight: "18px"
                    }}
                  />
                  <button
                    style={{
                      width: "30px",
                      height: "30px",
                      backgroundColor: "rgba(95, 109, 255, 0.4)",
                      border: "none",
                      borderRadius: "50%",
                      marginRight: "18px"
                    }}
                  />
                  <button
                    style={{
                      width: "30px",
                      height: "30px",
                      backgroundColor: "rgba(255, 161, 95, 0.4)",
                      border: "none",
                      borderRadius: "50%"
                    }}
                  />{" "}
                  {/* <br /> */}
                  <button
                    style={{
                      width: "30px",
                      height: "30px",
                      backgroundColor: "rgba(61, 61, 63, 0.4)",
                      border: "none",
                      borderRadius: "50%",
                      marginRight: "18px"
                    }}
                  />
                  <button
                    style={{
                      width: "30px",
                      height: "30px",
                      backgroundColor: "rgba(237, 237, 237, 0.4)",
                      border: "none",
                      borderRadius: "50%",
                      marginRight: "18px"
                    }}
                  />
                </div>
              </div>
            </div>

            <hr
              style={{
                width: "180px",
                height: "0.1px",
                border: "solid 0.5px #cccccc",
                margin: "9px 0 8.9px 0"
              }}
            />

            <div className="filter-brand">
              Brand <img src="/img/arrow2.svg" style={{ float: "right" }} />
              <div id="brand-drop">
                <hr
                  style={{
                    margin: "8px 0 14.9px 0",
                    width: "180px",
                    height: "0.1px",
                    border: "dashed 0.5px #cccccc"
                  }}
                />
                <div
                  style={{
                    // flexWrap: "nowrap", display: "flex"
                    lineHeight: "1.5"
                  }}
                >
                  <div className="brand-type">
                    <p>Zara</p>
                    <img src="/img/check-box.svg" style={{ float: "right" }} />
                  </div>
                  <div className="brand-type">
                    <p>H&M</p>
                    <img src="/img/check-box.svg" style={{ float: "right" }} />
                  </div>
                  <div className="brand-type">
                    <p>Pull&Bear</p>
                    <img src="/img/check-box.svg" style={{ float: "right" }} />
                  </div>
                  <div className="brand-type">
                    <p>Dior</p>
                    <img src="/img/check-box.svg" style={{ float: "right" }} />
                  </div>
                  <div className="brand-type">
                    <p>Channel</p>
                    <img src="/img/check-box.svg" style={{ float: "right" }} />
                  </div>
                </div>
              </div>
            </div>

            <hr
              style={{
                width: "180px",
                height: "0.1px",
                border: "solid 0.5px #cccccc",
                margin: "9px 0 8.9px 0"
              }}
            />

            <div className="filter-price">
              Price <img src="/img/arrow2.svg" style={{ float: "right" }} />
              <div id="price-drop">
                <hr
                  style={{
                    margin: "8px 0 14.9px 0",
                    width: "180px",
                    height: "0.1px",
                    border: "dashed 0.5px #cccccc"
                  }}
                />
                <div style={{}}>There should be a slider here</div>
              </div>
            </div>

            <hr
              style={{
                width: "180px",
                height: "0.1px",
                border: "solid 0.5px #cccccc",
                margin: "9px 0 8.9px 0"
              }}
            />

            <div className="filter-available">
              Available <img src="/img/arrow2.svg" style={{ float: "right" }} />
              <div id="available-drop">
                <hr
                  style={{
                    margin: "8px 0 14.9px 0",
                    width: "180px",
                    height: "0.1px",
                    border: "dashed 0.5px #cccccc"
                  }}
                />
                <div style={{}}>
                  <div className="brand-type">
                    <p>In-store</p>
                    <img src="/img/check-box.svg" style={{ float: "right" }} />
                  </div>
                  <div className="brand-type">
                    <p>Out of stock</p>
                    <img src="/img/check-box.svg" style={{ float: "right" }} />
                  </div>
                </div>
              </div>
            </div>

            <hr
              style={{
                width: "180px",
                height: "0.1px",
                border: "solid 0.5px #cccccc",
                margin: "9px 0 8.9px 0"
              }}
            />
          </div>

          <div className="clothes">{renderProduct}</div>
        </div>
        <br />
        <br />
        <Footer />
      </div>
    );
  }
}

export default withTracker(() => {
  const productHandle = Meteor.subscribe("products");
  const loading = !productHandle.ready();
  const products = !loading && Products.find();
  return {
    products: products ? products.fetch() : []
  };
})(Productlist);
