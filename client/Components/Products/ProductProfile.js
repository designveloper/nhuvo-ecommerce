import React, { Component } from "react";
import Header from "../../partial/Header";
import Footer from "../../partial/Footer";
import { withTracker } from "meteor/react-meteor-data";
import { Meteor } from "meteor/meteor";
import "../../../public/views/ProductProfile";
import { ProductDetails } from "../../../imports/api/productDetails";
import { Products } from "../../../imports/api/products";
import { Carts } from "../../../imports/api/carts";

class ProductProfile extends Component {
  constructor() {
    super();
    this.state = {
      sizeS: false,
      sizeM: false,
      sizeL: false,
      sizeLDisable: false,
      name: "",
      description: "",
      price: undefined,
      smallPic: [],
      brand: "",
      size1: undefined,
      size2: undefined,
      size3: undefined,
      color: undefined,
      chooseSize: "",
      chooseColor: "",
      chooseQuantity: 1
    };
  }

  componentDidMount() {
    const self = this;
    const url_id = this.props.match.params.product_id;
    // console.log("product id: ", url_id);
    // const id=[{url_id}]
    Meteor.call("productProfile.findOne", url_id, function(err, res) {
      if (err) {
        console.log(err);
      } else {
        console.log("get detail successful", res);
        self.setState({
          description: res.description,
          brand: res.brand,
          size1: res.size.S,
          size2: res.size.M,
          size3: res.size.L,
          color: res.color
        });
      }
    });

    Meteor.call("product.findOne", url_id, function(err, res) {
      if (err) {
        console.log(err);
      } else {
        // console.log("get big detail: ", res);
        self.setState({
          name: res.name,
          smallPic: res.image,
          price: res.price
        });
      }
    });
  }

  handleClick = params => event => {
    const self = this;
    // event.preventDefaul();
    if (params === "remove") {
      if (this.state.chooseQuantity > 1) {
        this.setState({
          chooseQuantity: this.state.chooseQuantity - 1
        });
      }
    }

    if (params === "add") {
      if (this.state.chooseSize === "S") {
        if (this.state.chooseQuantity < this.state.size1) {
          this.setState({
            chooseQuantity: this.state.chooseQuantity + 1
          });
        }
      } else if (this.state.chooseSize === "M") {
        if (this.state.chooseQuantity < this.state.size2) {
          this.setState({
            chooseQuantity: this.state.chooseQuantity + 1
          });
        }
      } else if (this.state.chooseSize === "L") {
        if (this.state.chooseQuantity < this.state.size3) {
          this.setState({
            chooseQuantity: this.state.chooseQuantity + 1
          });
        }
      }
    }

    if (params === "addToCart") {
      if (!Meteor.user()) {
        alert("Please log in first");
      } else {
        const item = [
          {
            product_name: this.state.name,
            product_id: this.props.match.params.product_id,
            price: this.state.price,
            color: this.state.chooseColor,
            quantity: this.state.chooseQuantity,
            size: this.state.chooseSize,
            user_id: Meteor.userId(),
            image: this.state.smallPic[0]
          }
        ];
        Meteor.call("carts.insert", item, function(err, res) {
          if (err) {
            console.log(err);
            alert("something happened. cannot add to cart");
          } else {
            alert("add to cart successfully");
            window.location.reload()
          }
        });
      }
    }
  };

  _renderColor = () => {
    return this.state.color
      ? this.state.color.map((color, i) => {
          return (
            <button
              key={i}
              style={{
                // width: "30px",
                // height: "30px",
                backgroundColor: color
                // border: "solid 1px lightgrey",
                // borderRadius: "50%",
                // marginRight: "18px"
              }}
              className="color-button"
              onClick={() =>
                this.setState({
                  chooseColor: color
                })
              }
            />
          );
        })
      : null;
  };

  render() {
    console.log(Meteor.userId());
    // console.log("choose size: ", this.state.chooseSize);
    // console.log("choose color: ", this.state.chooseColor);
    return (
      <div>
        <Header />
        <div className="ladies-dress">
          <div className="page-route">Ladies/ Dresses/ {this.state.name}</div>

          <div className="product-detail">
            <div className="small-images">
              <img src={this.state.smallPic[0]} />
              <img src={this.state.smallPic[1]} />
              <img src={this.state.smallPic[2]} />
              <img src={this.state.smallPic[3]} />
            </div>
            <div className="big-image">
              {/* <img src="/img/modal1.jpg" /> */}
              <img src={this.state.smallPic[0]} />
            </div>
            <div className="smaller-details">
              <div className="detail-title">
                {/* Collete Stretch Linen Minidress */}
                {this.state.name}
              </div>
              <div className="detail-price">
                {/* $69.00 */}${this.state.price}
              </div>
              <div>
                <img src="/img/star.svg" alt="star" />
                <img src="/img/star.svg" alt="star" />
                <img src="/img/star.svg" alt="star" />
                <img src="/img/star.svg" alt="star" />
                <img src="/img/star.svg" alt="star" />{" "}
                <span style={{ fontSize: "12px" }}>| 0 review</span>
              </div>
              <div style={{ margin: "25px 0 28px 0" }}>
                <p
                  style={{
                    fontSize: "14px",
                    margin: "0 0 9px 0",
                    fontWeight: "600"
                  }}
                >
                  Size
                </p>
                <div style={{ flexWrap: "nowrap", display: "flex" }}>
                  {this.state.sizeS && this.state.size1 > 0 ? (
                    <button
                      style={{
                        width: "40px",
                        height: "40px",
                        border: "none",
                        backgroundColor: "var(--pale-orange)",
                        color: "white",
                        fontSize: "12px"
                      }}
                      onClick={() =>
                        this.setState({ sizeS: false, chooseSize: "" })
                      }
                    >
                      S
                    </button>
                  ) : this.state.size1 > 0 ? (
                    <button
                      onClick={() =>
                        this.setState({
                          sizeS: true,
                          chooseSize: "S",
                          chooseQuantity: 1
                        })
                      }
                      className="size-button"
                    >
                      S
                    </button>
                  ) : (
                    <button
                      style={{
                        width: "40px",
                        height: "40px",
                        border: "solid 1px var(--white-four)",
                        // marginLeft: "16px",
                        background: "transparent",
                        opacity: "0.6",
                        fontSize: "12px",
                        cursor: "not-allowed"
                      }}
                    >
                      S
                    </button>
                  )}
                  {this.state.sizeM && this.state.size2 > 0 ? (
                    <button
                      style={{
                        width: "40px",
                        height: "40px",
                        border: "none",
                        marginLeft: "16px",
                        backgroundColor: "var(--pale-orange)",
                        color: "white",
                        fontSize: "12px"
                      }}
                      onClick={() =>
                        this.setState({ sizeM: false, chooseSize: "" })
                      }
                    >
                      M
                    </button>
                  ) : this.state.size2 > 0 ? (
                    <button
                      style={{
                        marginLeft: "16px"
                      }}
                      className="size-button"
                      onClick={() =>
                        this.setState({
                          sizeM: true,
                          chooseSize: "M",
                          chooseQuantity: 1
                        })
                      }
                    >
                      M
                    </button>
                  ) : (
                    <button
                      style={{
                        width: "40px",
                        height: "40px",
                        border: "solid 1px var(--white-four)",
                        marginLeft: "16px",
                        background: "transparent",
                        opacity: "0.6",
                        fontSize: "12px",
                        cursor: "not-allowed"
                      }}
                    >
                      M
                    </button>
                  )}
                  {this.state.sizeL && this.state.size3 > 0 ? (
                    <button
                      style={{
                        width: "40px",
                        height: "40px",
                        border: "none",
                        marginLeft: "16px",
                        backgroundColor: "var(--pale-orange)",
                        color: "white",
                        fontSize: "12px"
                      }}
                      onClick={() =>
                        this.setState({ sizeL: false, chooseSize: "" })
                      }
                    >
                      L
                    </button>
                  ) : this.state.size3 > 0 ? (
                    <button
                      style={{
                        marginLeft: "16px"
                      }}
                      className="size-button"
                      onClick={() =>
                        this.setState({
                          sizeL: true,
                          chooseSize: "L",
                          chooseQuantity: 1
                        })
                      }
                    >
                      L
                    </button>
                  ) : (
                    <button
                      style={{
                        width: "40px",
                        height: "40px",
                        border: "solid 1px var(--white-four)",
                        marginLeft: "16px",
                        background: "transparent",
                        opacity: "0.6",
                        fontSize: "12px",
                        cursor: "not-allowed"
                      }}
                    >
                      L
                    </button>
                  )}
                </div>
              </div>
              <div>
                <p
                  style={{
                    fontSize: "14px",
                    margin: "0 0 9px 0",
                    fontWeight: "600"
                  }}
                >
                  Color
                </p>
                {this._renderColor()}
              </div>{" "}
              <div className="quanlity">
                Quantity
                {this.state.chooseSize ? (
                  <div>
                    <img
                      src="/img/minus.svg"
                      onClick={this.handleClick("remove")}
                    />
                    <span>{this.state.chooseQuantity}</span>
                    <img
                      src="/img/plus.svg"
                      onClick={this.handleClick("add")}
                    />
                  </div>
                ) : (
                  <div>
                    <img
                      src="/img/minus.svg"
                      style={{ cursor: "not-allowed" }}
                    />
                    <span>1</span>
                    <img
                      src="/img/plus.svg"
                      style={{ cursor: "not-allowed" }}
                    />
                  </div>
                )}
                {this.state.chooseSize == "" || this.state.chooseColor == "" ? (
                  <span style={{ color: "red", paddingLeft: "5px" }}>
                    Please choose size & color first!
                  </span>
                ) : null}
              </div>
              {this.state.chooseSize != "" && this.state.chooseColor != "" ? (
                <button
                  className="add-to-cart"
                  onClick={this.handleClick("addToCart")}
                >
                  Add to cart
                </button>
              ) : (
                <button
                  className="add-to-cart"
                  style={{
                    backgroundColor: "var(--white-four)",
                    cursor: "not-allowed"
                  }}
                >
                  Add to cart
                </button>
              )}
              <hr />
              <p
                style={{
                  fontSize: "12px",
                  overflow: "auto",
                  height: "70px"
                }}
              >
                {this.state.description}
              </p>
            </div>
            <div className="suggestion">
              <div>
                <p style={{ fontWeight: "600" }}>More from</p>
                <p>{this.state.brand}</p>
              </div>
              <img src="/img/modal2.jpg" />
              <img src="/img/modal3.jpg" />
              <img src="/img/modal4.jpg" />
              <img src="/img/modal5.jpg" />
            </div>
          </div>

          <div className="review-line">
            <div>
              <hr style={{ width: "90px" }} />
            </div>
            <div>Reviews</div>

            <div>
              <hr style={{ width: "1000px" }} />
            </div>
          </div>

          <div className="no-review">
            <p>No reviews</p>
          </div>

          <div className="review-line">
            <div>
              <hr style={{ width: "90px" }} />
            </div>
            <div>You may also like</div>

            <div>
              <hr style={{ width: "1000px" }} />
            </div>
          </div>

          <div className="also-like">
            <div>
              <img src="/img/modal6.jpg" alt="also like" />
              <p>Collete Stretch Linen Minidress</p>
            </div>
            <div>
              <img src="/img/modal7.jpg" alt="also like" />
              <p>Collete Stretch Linen Minidress</p>
            </div>
            <div>
              <img src="/img/modal8.jpg" alt="also like" />
              <p>Collete Stretch Linen Minidress</p>
            </div>
            <div>
              <img src="/img/modal9.jpg" alt="also like" />
              <p>Collete Stretch Linen Minidress</p>
            </div>
            <div>
              <img src="/img/modal10.jpg" alt="also like" />
              <p>Collete Stretch Linen Minidress</p>
            </div>
            <div>
              <img src="/img/modal11.jpg" alt="also like" />
              <p>Collete Stretch Linen Minidress</p>
            </div>
            <div>
              <img src="/img/modal12.jpg" alt="also like" />
              <p>Collete Stretch Linen Minidress</p>
            </div>
            <div>
              <img src="/img/modal4.jpg" alt="also like" />
              <p>Collete Stretch Linen Minidress</p>
            </div>
          </div>
        </div>
        {/* {console.log("profile of products ", this.props.productProfile)} */}
        <Footer />
      </div>
    );
  }
}

export default withTracker(() => {
  const productHandle = Meteor.subscribe("productDetails");
  const loading = !productHandle.ready();
  const productProfile = !loading && ProductDetails.find();

  const productHandle2 = Meteor.subscribe("products");
  const loading2 = !productHandle2.ready();
  const products = !loading2 && Products.find();

  const productHandle3 = Meteor.subscribe("carts");
  const loading3 = !productHandle3.ready();
  const carts = !loading3 && Carts.find();
  return {
    productProfile: productProfile ? productProfile.fetch() : [],
    products: products ? products.fetch() : [],
    carts: carts ? carts.fetch() : []
  };
})(ProductProfile);
