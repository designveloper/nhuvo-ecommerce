import React, { Component } from "react";
import { withTracker } from "meteor/react-meteor-data";
import { Meteor } from "meteor/meteor";
import { Carts } from "../../../imports/api/carts";
import Footer from "../../partial/Footer";
import Header from "../../partial/Header";
import "../../../public/views/CartCheckout";

class CartCheckout extends Component {
  constructor() {
    super();
    this.state = {
      cartItem: [],
      totalPrice: 0,
      checkoutDetail: ""
    };
  }

  componentDidMount() {
    const self = this;
    Meteor.call("cart.find", Meteor.userId(), function(err, res) {
      if (err) {
        console.log(err);
        alert("cannot display cart's items");
      } else {
        console.log(res);
        self.setState({ cartItem: res });
        Object.keys(res).map((item, i) => {
          self.setState({
            totalPrice:
              self.state.totalPrice + res[item].price * res[item].quantity,
            checkoutDetail:`${self.state.checkoutDetail} ${res[item].product_name} (${res[item].size}) x ${res[item].quantity} ${res[item].color}.`
              
          });
        });
      }
    });
  }

  handleClick = param => event => {
    // const self =  this
    Meteor.call("cart.remove", param, function(err, res) {
      if (err) {
        console.log(err);
        alert("cannot remove");
      } else {
        window.location.reload();
      }
    });
  };

  handleCheckout = () => {
    const self = this;
    let err;
    err = false;
    if (this.state.cartItem.length === 0) {
      alert("you have no item in cart");
    } else {
      Object.keys(this.state.cartItem).map((item, i) => {
        const buySize = this.state.cartItem[item].size;
        Meteor.call(
          "productProfile.findOne",
          self.state.cartItem[item].product_id,
          function(err, res) {
            if (err) {
              console.log(err);
              err = true;
            } else {
              if (self.state.cartItem[item].quantity > res.size[buySize]) {
                alert(
                  self.state.cartItem[item].product_name,
                  " does not have enough item in stock, please change quantity!"
                );
              } else {
                let minusS, minusM, minusL;

                if (self.state.cartItem[item].size === "S") {
                  minusS = self.state.cartItem[item].quantity;
                  (minusM = 0), (minusL = 0);
                } else if (self.state.cartItem[item].size === "M") {
                  minusM = self.state.cartItem[item].quantity;
                  (minusS = 0), (minusL = 0);
                } else {
                  minusL = self.state.cartItem[item].quantity;
                  (minusM = 0), (minusS = 0);
                }
                console.log("S: ", minusS, " M ", minusM, " L ", minusL);
                const product = {
                  id: self.state.cartItem[item].product_id,
                  quantity: self.state.cartItem[item].quantity,
                  sizeS: minusS,
                  sizeM: minusM,
                  sizeL: minusL
                };

                Meteor.call("productProfile.update", product, function(
                  err,
                  res
                ) {
                  if (err) {
                    console.log(err);
                  } else {
                    alert("productProdifle updated!");
                  }
                });

                Meteor.call(
                  "cart.remove",
                  self.state.cartItem[item]._id,
                  function(err, res) {
                    if (err) {
                      console.log(err);
                    } else {
                      alert("product removed from cart");
                    }
                  }
                );
              }
            }
          }
        );
      });

      Meteor.call(
        "orders.insert",
        [
          {
            detail: self.state.checkoutDetail,
            total: self.state.totalPrice,
            status: 0,
            buyer_id: Meteor.userId(),
            createdAt: new Date()
          }
        ],
        function(err, res) {
          if (err) {
            console.log(err);
          } else {
            alert("order submited!");
          }
        }
      );
    }
  };

  _renderCheckout = () => {
    return Object.keys(this.state.cartItem).map((item, i) => {
      const url = "/product-list/" + this.state.cartItem[item].product_id;
      const _id = this.state.cartItem[item]._id;
      return (
        <div key={i}>
          <div className="product-discription">
            <div className="buy-data">
              <div className="buy-data-img">
                <img src={this.state.cartItem[item].image} />
              </div>
              <div className="buy-data-container">
                <div className="buy-data-name">
                  <a href={url}>{this.state.cartItem[item].product_name}</a>
                </div>
                <div className="buy-data-button">
                  <span>
                    <a href={url}>Change</a>
                  </span>{" "}
                  | <span onClick={this.handleClick(_id)}> Remove</span>
                </div>
              </div>
            </div>
            <div className="buy-color">
              {" "}
              <button
                style={{
                  width: "30px",
                  height: "30px",
                  backgroundColor: this.state.cartItem[item].color,
                  border: "solid 1px lightgrey",
                  borderRadius: "50%"
                }}
              />
            </div>
            <div className="buy-size">{this.state.cartItem[item].size}</div>
            <div className="buy-quantity">
              <img src="/img/minus.svg" />
              <span> {this.state.cartItem[item].quantity}</span>
              <img src="/img/plus.svg" />
            </div>
            <div className="buy-price">
              $
              {this.state.cartItem[item].price *
                this.state.cartItem[item].quantity}
            </div>
          </div>
          <hr
            style={{
              margin: "10px 0 9.5px 0",
              color: "lightgrey"
            }}
          />
        </div>
      );
    });
  };

  render() {
    console.log(this.state.checkoutDetail);
    return (
      <div>
        <Header />
        <div className="shop-area">
          <div className="my-bag">My Bag</div>
          <div className="buy-product">
            <div className="buy-detail">
              <div className="bag-title">
                <div className="shop-title" style={{ width: "209px" }}>
                  Product
                </div>
                <div className="shop-title" style={{ width: "40px" }}>
                  Color
                </div>
                <div className="shop-title" style={{ width: "30px" }}>
                  Size
                </div>
                <div
                  className="shop-title"
                  style={{ width: "108px", paddingLeft: "20px" }}
                >
                  Quantity
                </div>
                <div className="shop-title" style={{ width: "61px" }}>
                  Amount
                </div>
              </div>
              <hr
                style={{
                  margin: "10px 0 9.5px 0",
                  color: "lightgrey"
                }}
              />
              <div style={{ height: "521px", overflow: "auto" }}>
                {this._renderCheckout()}
              </div>
            </div>{" "}
            <div className="price-detail">
              {" "}
              {/* display price */}
              <p className="shop-title" style={{ marginBottom: "10px" }}>
                Total
              </p>
              <div className="shop-checkout-detail">
                <div className="shop-detail-small">
                  <div className="shop-small">
                    <h5>Shipping & Handling:</h5>
                    <h5>Free</h5>
                  </div>
                  <div className="shop-small">
                    <h5>Total product:</h5>
                    <h5>${this.state.totalPrice}</h5>
                  </div>
                  <hr style={{ margin: "15px 0 4px 0" }} />
                  <div
                    className="shop-small"
                    style={{ fontSize: "16px", fontWeight: "bold" }}
                  >
                    <h5 style={{ fontSize: "16px", fontWeight: "bold" }}>
                      Subtotal
                    </h5>
                    <h5 style={{ fontSize: "16px", fontWeight: "bold" }}>
                      ${this.state.totalPrice}
                    </h5>
                  </div>
                </div>
              </div>
              {Meteor.userId() ? (
                <button
                  className="checkout-button"
                  onClick={this.handleCheckout}
                >
                  Check out
                </button>
              ) : (
                <button
                  className="checkout-button-disable"
                  onClick={() => alert("login first")}
                >
                  Check out
                </button>
              )}
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default withTracker(() => {
  const cartHandle = Meteor.subscribe("carts");
  const loading = !cartHandle.ready();
  const cartList = !loading && Carts.find();

  return {
    cartList: cartList ? cartList.fetch() : []
  };
})(CartCheckout);
