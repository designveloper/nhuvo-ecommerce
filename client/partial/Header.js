import React, { Component } from "react";
import Menu from "./Menu";
import Modal from "react-bootstrap/Modal";
import "/public/views/Header.css";
import { withTracker } from "meteor/react-meteor-data";
import { Meteor } from "meteor/meteor";
// import { Accounts } from "meteor/accounts-base";
import { check } from "meteor/check";
import Cart from "./Cart";

class Header extends Component {
  constructor() {
    super();
    this.state = {
      login: false,
      forgotPass: false,
      register: false,
      name: "",
      email: "",
      pass: "",
      loginEmail: "",
      loginPass: "",
      isLoggedIn: null,
      isLoggedIn2: false
    };
    this.submitRegister = this.submitRegister.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    console.log(Meteor.userId());
    this.setState({
      isLoggedIn: Meteor.userId()
    });
  }

  handleChange(event) {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }

  submitRegister(event) {
    event.preventDefault();
    const self = this;
    const user = [
      {
        email: this.state.email,
        password: this.state.pass,
        profile: {
          name: {
            fullName: this.state.name
          }
        }
      }
    ];

    Meteor.call("users.insert", user, function(error, result) {
      if (error) {
        console.log(error);
        alert(error.error);
      } else {
        alert("register sucessfully! Now let's login");
        self.setState({ login: true, register: false });
      }
    });
  }

  submitLogin = event => {
    event.preventDefault();
    const self = this;
    const users = [
      {
        email: this.state.loginEmail,
        password: this.state.loginPass
      }
    ];

    check(users, [
      {
        email: String,
        password: String
      }
    ]);
    users.forEach(({ email, password }) => {
      Meteor.loginWithPassword(email, password, function(err) {
        if (err) {
          alert(err.reason);
        } else {
          alert("login successfully!");
          self.setState({ login: false, isLoggedIn2: true });
        }
      });
    });
  };

  render() {
    return (
      <div className="header1  !important">
        <div className="upper-header  !important">
          <div className="upper1  !important">
            <form
              style={{
                alignItems: "center",
                justifyContent: "center",
                display: "flex"
              }}
            >
              <input
                type="text"
                className="search  !important"
                placeholder="Search"
              />
            </form>
            <a
              href="/"
              style={{
                alignItems: "center",
                justifyContent: "center",
                display: "flex"
              }}
            >
              <img src="/img/logo.svg" />
            </a>
            <div style={{ display: "flex", alignItems: "center" }}>
              {this.state.isLoggedIn != null || this.state.isLoggedIn2 ? (
                <div
                  style={{ paddingRight: "19px", paddingLeft: "150px" }}
                  className="profile-ava"
                >
                  <img src="/img/modal2.jpg" className="profile-minipic" />
                  <div id="ava-dropdown">
                    <div className="ava-profile">
                      <a href="/profile">Account setting</a>
                    </div>
                    {/* <hr style={{margin: "0 0 0 0"}}/> */}
                    <div
                      className="ava-logout"
                      onClick={async () => {
                        await Meteor.logout();
                        if (this.state.isLoggedIn2){
                          this.setState({
                            isLoggedIn2: false
                          });
                        } else{
                          window.location.reload()
                        }
                      }}
                    >
                      Log out
                    </div>
                  </div>
                </div>
              ) : (
                <div>
                  <p
                    className="register1  !important"
                    onClick={() => this.setState({ register: true })}
                  >
                    Register
                  </p>
                  <div style={{ paddingRight: "19px" }}>
                    <button
                      type="button"
                      className="login"
                      onClick={() => this.setState({ login: true })}
                    >
                      Log In
                    </button>
                  </div>
                </div>
              )}

              <div className="cart">
                <img src="/img/cart.svg" alt="cart" />
                <div id="cart-list">
                  <Cart />
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="ruler1  !important">
          <hr style={{ margin: "8px 0 8px 0" }} />
        </div>

        <div className="lower-header  !important">
          <div className="lower11  !important">
            <div className="dropdownn  !important">
              <p>Men</p>

              <div className="arrow">
                <img
                  src="/img/arrow.svg"
                  alt="arrow"
                  style={{ paddingRight: "19px" }}
                />
                <div id="menu-ladies">
                  <Menu type="men" />
                </div>
              </div>

              <p>Ladies</p>
              <div className="arrow">
                <img
                  src="/img/arrow.svg"
                  alt="arrow"
                  style={{ paddingRight: "19px" }}
                />
                <div id="menu-ladies">
                  <Menu type="ladies" />
                </div>
              </div>
              <p>Girls</p>
              <img
                src="/img/arrow.svg"
                alt="arrow"
                className="arrow1 "
                style={{ paddingRight: "19px" }}
              />
              <p>Boys</p>
              <img src="/img/arrow.svg" alt="arrow" className="arrow1" />
            </div>
          </div>
        </div>

        {/* <Menu modal={this.state.modal} /> */}
        <Modal //login modal
          show={this.state.login}
          onHide={() => {
            this.setState({ login: false });
          }}
          centered
        >
          <Modal.Body
            style={{
              padding: "0 0 0 0",
              width: "555px",
              height: "568px",
              boxShadow: "0 14px 30px 0 rgba(0, 0, 0, 0.14)",
              backgroundColor: "var(--white)"
            }}
          >
            <img
              src="/img/cross.svg"
              alt="close"
              className="close-log"
              onClick={() => this.setState({ login: false })}
            />
            <form onSubmit={this.submitLogin}>
              <div className="login-tittle">Log In</div>
              <div className="mail-pass" style={{ marginTop: "35px" }}>
                E-MAIL
              </div>
              <input
                type="text"
                placeholder="Enter your email..."
                value={this.state.loginEmail}
                name="loginEmail"
                onChange={this.handleChange}
                className="login-box"
              />
              <div className="mail-pass" style={{ marginTop: "24px" }}>
                PASSWORD
              </div>
              <input
                type="password"
                placeholder="Enter your password..."
                value={this.state.loginPass}
                name="loginPass"
                onChange={this.handleChange}
                className="login-box"
              />
              <br />
              <div className="login-footer">
                <input type="checkbox" className="checkbox-login" />

                <p className="remem">Remember password</p>
                <p
                  className="forgot"
                  onClick={() =>
                    this.setState({ forgotPass: true, login: false })
                  }
                >
                  Forgot your password?
                </p>
              </div>

              <button type="submit" className="login-button">
                Log In
              </button>
            </form>
            <hr
              style={{
                padding: "0 0 0 0",
                opacity: "0.1",
                backgroundColor: "var(--dark-grey)",
                margin: "68px 0 0 0"
              }}
            />
            <div className="no-acc">
              <p className="dont-have">Don't have an account?</p>
              <p
                className="regist"
                onClick={() => this.setState({ register: true, login: false })}
              >
                Register
              </p>
            </div>
          </Modal.Body>
        </Modal>

        <Modal //forgot password
          show={this.state.forgotPass}
          onHide={() => {
            this.setState({ forgotPass: false });
          }}
          centered
        >
          <Modal.Body
            style={{
              padding: "0 0 0 0",
              width: "555px",
              height: "436px",
              boxShadow: "0 14px 30px 0 rgba(0, 0, 0, 0.14)",
              backgroundColor: "var(--white)"
            }}
          >
            <img
              src="/img/cross.svg"
              alt="close"
              className="close-log"
              onClick={() => this.setState({ forgotPass: false })}
            />
            <div className="login-tittle2">Forgot Password</div>
            <p className="forgot-discript">
              Enter your e-mail address below and we'll get you back on track.
            </p>
            <div className="mail-pass" style={{ marginTop: "34px" }}>
              E-MAIL
            </div>
            <input
              type="text"
              placeholder="Enter your email..."
              className="login-box"
            />

            <button type="button" className="submit-button">
              Submit
            </button>
            <hr
              style={{
                padding: "0 0 0 0",
                opacity: "0.1",
                backgroundColor: "var(--dark-grey)",
                margin: "68px 0 0 0"
              }}
            />
            <div className="no-acc">
              <p className="dont-have">I remember my password now.</p>
              <p
                className="regist"
                onClick={() =>
                  this.setState({ forgotPass: false, login: true })
                }
              >
                Log In
              </p>
            </div>
          </Modal.Body>
        </Modal>

        <Modal //register modal
          show={this.state.register}
          onHide={() => {
            this.setState({ register: false });
          }}
          centered
        >
          <Modal.Body
            style={{
              padding: "0 0 0 0",
              width: "555px",
              height: "703px",
              boxShadow: "0 12px 24px 0 rgba(0, 0, 0, 0.03)",
              backgroundColor: "var(--white)"
            }}
          >
            <img
              src="/img/cross.svg"
              alt="close"
              className="close-log"
              onClick={() => this.setState({ register: false })}
            />
            <div className="login-tittle">Register</div>
            <form onSubmit={this.submitRegister}>
              <div className="mail-pass" style={{ marginTop: "35px" }}>
                NAME
              </div>
              <input
                type="text"
                name="name"
                placeholder="Enter your name..."
                className="login-box"
                value={this.state.name}
                onChange={this.handleChange}
              />
              <div className="mail-pass" style={{ marginTop: "24px" }}>
                E-MAIL
              </div>
              <input
                type="text"
                name="email"
                placeholder="Enter your email..."
                className="login-box"
                value={this.state.email}
                onChange={this.handleChange}
              />
              <div className="mail-pass" style={{ marginTop: "24px" }}>
                PASSWORD
              </div>
              <input
                type="password"
                name="pass"
                placeholder="Enter your password..."
                className="login-box"
                value={this.state.pass}
                onChange={this.handleChange}
              />
              <br />
              <div className="login-footer" style={{ paddingRight: "80px" }}>
                <div className="terms">
                  <p className="agree">
                    By creating an account you agree to the
                  </p>
                  <p
                    className="highlight-term"
                    onClick={() => alert("There is no term :)")}
                  >
                    Terms of Service
                  </p>
                  <p style={{ opacity: "0" }}>i</p>
                  <p className="agree">and</p>
                  <p style={{ opacity: "0" }}>i</p>
                  <p
                    className="highlight-term"
                    onClick={() => alert("There is no policy :)")}
                  >
                    Privacy Policy
                  </p>
                </div>
              </div>
              <button
                type="submit"
                className="login-button"
                style={{ backgroundColor: "var(--pale-orange)" }}
              >
                Register
              </button>
            </form>
            <hr
              style={{
                padding: "0 0 0 0",
                opacity: "0.1",
                backgroundColor: "var(--dark-grey)",
                margin: "68px 0 0 0"
              }}
            />
            <div className="no-acc">
              <p className="dont-have">Do you have an account?</p>
              <p
                className="regist"
                onClick={() => this.setState({ login: true, register: false })}
              >
                Log In
              </p>
            </div>
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}

export default withTracker(() => {
  Meteor.subscribe("users");
  return {};
})(Header);
