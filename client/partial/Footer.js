import React, { Component } from "react";
import "/public/views/Footer.css";
// import img from "/logo.svg";

class Footer extends Component {
  render() {
    return (
      <div className="footer">
        <div className="upper_footer">
          <div className="upper1">
            <img
              src="/img/logo.svg"
              alt="logo"
              style={{ width: "96px", height: "20px" }}
            />
            <div className="list">
              <a href="/">
                <p style={{ paddingRight: "36px" }}>Home</p>
              </a>
              <a href="/product-list">
                <p style={{ paddingRight: "36px" }}>Products</p>
              </a>
              <p style={{ paddingRight: "36px", cursor: "pointer" }}>
                Services
              </p>
              <p style={{ paddingRight: "36px", cursor: "pointer" }}>
                About us
              </p>
              <p style={{ paddingRight: "36px", cursor: "pointer" }}>Help</p>
              <p>Contacts</p>
            </div>
            <div>
              <img
                style={{ paddingRight: "15px", cursor: "pointer" }}
                src="/img/twitter-icon.svg"
                alt="twitter_icon"
              />
              <img
                style={{ paddingRight: "15px", cursor: "pointer" }}
                src="/img/facebook-icon.svg"
                alt="twitter_icon"
              />
              <img
                src="/img/instagram-6-icon.svg"
                alt="twitter_icon"
                style={{ cursor: "pointer" }}
              />
            </div>
          </div>
        </div>

        <div className="rulerclass">
          <hr className="ruler" />
        </div>

        <div className="lower-footer">
          <div className="lower1">
            <div className="list" style={{ float: "left" }}>
              <a href="/">
                <p style={{ paddingRight: "36px" }}>Home</p>
              </a>
              <a href="/product-list">
                <p style={{ paddingRight: "36px" }}>Products</p>
              </a>
              <p style={{ paddingRight: "36px", cursor: "pointer" }}>
                Services
              </p>
              <p style={{ paddingRight: "36px", cursor: "pointer" }}>
                About us
              </p>
              <p style={{ paddingRight: "36px", cursor: "pointer" }}>Help</p>
              <p>Contacts</p>
            </div>

            {/* <div>Hey therekfsl;fkds;</div> */}

            <div className="list">
              <p style={{ paddingRight: "36px" }}>Privacy Policy</p>
              <p>Terms & Conditions</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;
