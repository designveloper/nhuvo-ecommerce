import React, { Component } from "react";
import "../../public/views/Menu.css";

class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="ladies-menu">
        {this.props.type == "ladies" ? (
          <div className="ladies">
            <a href="/product-list/ladies/tops">
              <p>Tops</p>
            </a>
            <a href="/product-list/ladies/bottoms">
              <p>Bottoms</p>
            </a>
            <a href="/product-list/ladies/dress">
              <p>Dresses</p>
            </a>
            <a href="/product-list/ladies/jackets">
              <p>Jackets</p>
            </a>
            <a href="">
              <p>Shoes</p>
            </a>
            <a href="">
              <p>Accesories</p>
            </a>
            <a href="">
              <p>Sale</p>
            </a>
          </div>
        ) : (
          <div className="ladies">
            <a href="/product-list/men/tops">
              <p>Tops</p>
            </a>
            <a href="/product-list/men/bottoms">
              <p>Bottoms</p>
            </a>
            <a href="/product-list/men/suite-tie">
              <p>Suit&Tie</p>
            </a>
            <a href="/product-list/men/jackets">
              <p>Jackets</p>
            </a>
            <a href="">
              <p>Shoes</p>
            </a>
            <a href="">
              <p>Accesories</p>
            </a>
            <a href="">
              <p>Sale</p>
            </a>
          </div>
        )}
      </div>
    );
  }
}

export default Menu;
