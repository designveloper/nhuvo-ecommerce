import React, { Component } from "react";
import { withTracker } from "meteor/react-meteor-data";
import { Meteor } from "meteor/meteor";
import "../../public/views/Cart.css";
import { Carts } from "../../imports/api/carts";

class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cartItem: []
    };
  }
  
  componentDidMount() {
    const self = this;
    Meteor.call("cart.find", Meteor.userId(), function(err, res) {
      if (err) {
        console.log(err);
        alert("cannot display cart's items");
      } else {
        console.log(res);
        self.setState({ cartItem: res });
      }
    });
  }

  _renderCartBox = () => {
    return Object.keys(this.state.cartItem).map((item, i) => {
      let url;
      url = "/product-list/" + this.state.cartItem[item].product_id.toString();
      return (
        <div className="cart-small-box" key={i}>
          <div className="cart-img">
            <img src={this.state.cartItem[item].image} alt="cart-small" />
          </div>
          <div className="cart-info">
            <div className="cart-info-name">
              <a href={url}>{this.state.cartItem[item].product_name}</a>
            </div>
            <div className="cart-info-detail">
              <div className="cart-price">
                ${this.state.cartItem[item].price}
              </div>
              <div className="cart-small-detail">
                {this.state.cartItem[item].size} -{" "}
                {this.state.cartItem[item].color} -{" "}
                {this.state.cartItem[item].quantity}pcs
              </div>
            </div>
          </div>
        </div>
      );
    });
  };

  render() {
    return (
      <div className="cart-box">
        {this._renderCartBox()}
        <div className="view-cart">
          <p>
            <a href="/cart-checkout">View cart</a>
          </p>
        </div>{" "}
      </div>
    );
  }
}

export default withTracker(() => {
  const cartHandle = Meteor.subscribe("carts");
  const loading = !cartHandle.ready();
  const cartList = !loading && Carts.find();

  return {
    cartList: cartList ? cartList.fetch() : []
  };
})(Menu);
