import React from "react";
// import ReactDOM from "react-dom";
// import { createBrowserHistory } from "history";
import history from "history";
import { Route, Switch, BrowserRouter } from "react-router-dom";
import Homepage from "../client/Components/Home/Homepage";
import Productlist from "../client/Components/Products/Productlist";
import Profile from "../client/Components/Profile/Profile";
import ProductProfile from "../client/Components/Products/ProductProfile";
import CartCheckout from "../client/Components/Products/CartCheckout";
import Login from "../client/Components/Seller/Login";
import Page404 from "../client/Components/Page404/Page404";
import Order from "../client/Components/Seller/Order";

var hist = history.createBrowserHistory();

function Routes() {
  return (
    <BrowserRouter history={hist}>
      <Switch>
        <Route exact path="/admin" component={Login} />
        <Route exact path="/admin/order" component={Order} />
        <Route exact path="/cart-checkout" component={CartCheckout} />
        <Route
          exact
          path="/product-list/:type/:category"
          component={Productlist}
        />
        <Route path="/product-list/:product_id" component={ProductProfile} />
        <Route exact path="/profile" component={Profile} />
        <Route exact path="/" component={Homepage} />
        <Route component={Page404} />
      </Switch>
    </BrowserRouter>
  );
}

export default Routes;
