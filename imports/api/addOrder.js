import { Meteor } from "meteor/meteor";
import { check } from "meteor/check";
import { Orders } from "./orders";

if (Meteor.isServer) {
  Meteor.publish("orders", function usersPublication() {
    // return Users.find();
  });
}

Meteor.methods({
  "orders.insert"(item) {
    check(item, [
      {
        detail: String,
        total: Number,
        status: Number,
        buyer_id: String,
        createdAt: Date
      }
    ]);

    item.forEach(({ detail, total, status, createdAt,buyer_id }) => {
      Orders.insert(
        {
          detail,
          total,
          status,
          buyer_id,
          createdAt
        },
        function(err, id) {
          if (err) {
            console.log(err);
            throw new Meteor.Error("cannot create order");
          } else {
            return id;
          }
        }
      );
    });
  }
});
