import { Meteor } from "meteor/meteor";
import { check } from "meteor/check";
// import { ProductDetails } from "./productDetails";
import { Carts } from "./carts";

if (Meteor.isServer) {
  Meteor.publish("carts", function usersPublication() {
    // return Users.find();
  });
}

Meteor.methods({
  "carts.insert"(item) {
    check(item, [
      {
        product_name: String,
        product_id: String,
        price: Number,
        color: String,
        quantity: Number,
        size: String,
        user_id: String,
        image: String
      }
    ]);

    item.forEach(
      ({
        product_name,
        product_id,
        price,
        color,
        quantity,
        size,
        user_id,
        image
      }) => {
        Carts.insert(
          {
            product_name,
            product_id,
            price,
            color,
            quantity,
            size,
            user_id,
            image
          },
          function(err, id) {
            if (err) {
              console.log(err);
              throw new Meteor.Error("cannot add to cart");
            } else {
              return id;
            }
          }
        );
      }
    );
  },

  "cart.find"(product) {
    const cartExist = Carts.find({ user_id: product }).fetch();
    if (cartExist) {
      console.log("found product profile");
      return cartExist;
    } else {
      throw new Meteor.Error("this product has no detail");
    }
  },

  "cart.remove"(product) {
    const cartExist = Carts.remove({_id: product}, function(err,id){
      if (err){
        console.log(err);
        throw new Meteor.Error("cannot remove product!")
      } else {
        return
      }
    })

  }

});
