import { Meteor } from "meteor/meteor";
// import { Accounts } from "meteor/accounts-base";
import { Products } from "./products";
import { check } from "meteor/check";
import { ProductDetails } from "./productDetails";

if (Meteor.isServer) {
  Meteor.publish("products", function productsPublication() {
    return Products.find({});
  });
  Meteor.publish("productDetails", function productsPublication() {
    return ProductDetails.find({});
  });
}

Meteor.methods({
  "productProfile.findOne"(product) {
    const productExist = ProductDetails.findOne({ id: product });
    if (productExist) {
      console.log("found product profile");
      return productExist;
    } else {
      throw new Meteor.Error("this product has no detail");
    }
  },

  "product.findOne"(product) {
    const productExist = Products.findOne({ _id: product });
    if (productExist) {
      console.log("found product big details");
      return productExist;
    } else {
      throw new Meteor.Error("this product does not exist");
    }
  },

  "productProfile.update"(product) {
    const productUpate = ProductDetails.update(
      { id: product.id },
      {
        $inc: {
          inStock: -product.quantity,
          "size.S": -product.sizeS,
          "size.M": -product.sizeM,
          "size.L": -product.sizeL
        }
      },
      { multi: false },
      function(err, id) {
        if (err) {
          console.log(err);
          throw new Meteor.Error("cannot update Product detail");
        } else {
          return;
        }
      }
    );
  }
});
