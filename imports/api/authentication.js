import { Meteor } from "meteor/meteor";
import { Accounts } from "meteor/accounts-base";
import { check } from "meteor/check";
// import Users from "../../imports/api/users";

if (Meteor.isServer) {
  Meteor.publish("users", function usersPublication() {
    // return Users.find();
  });

  Meteor.publish("usersAdmin", function() {
    if (this.userId) {
      return Meteor.users.find(
        { _id: this.userId },
        {
          fields: { role: 1, profile: 1, email: 1 }
        }
      );
    } else {
      this.ready();
    }
  });

  // Meteor.publish("usersAdmin", function usersPublication() {
  //   const user = Meteor.user({
  //     field: {
  //       role: 1,
  //       profile: 1,
  //       email: 1
  //     }
  //   });
  //   console.log("check user:", user);
  //   if (user && user.role) {
  //     console.log("role:", user.role);
  //     return Meteor.user({
  //       field: {
  //         role: 1,
  //         profile: 1,
  //         email: 1
  //       }
  //     }).fetch();
  //   } else {
  //     throw new Meteor.Error("Un authorize admin!");
  //   }
  // });
}

Meteor.methods({
  "users.insert"(users) {
    check(users, [
      {
        email: String,
        password: String,
        profile: {
          name: {
            fullName: String
          }
        }
      }
    ]);

    users.forEach(({ email, password, profile }) => {
      const userExists = Accounts.findUserByEmail(email);

      if (!userExists) {
        const userId = Accounts.createUser({ email, password, profile });
        console.log("inserted!");
        return userId;
      } else {
        throw new Meteor.Error("user exists!");
      }
    });
  }

  // "users.login"(users) {
  //   check(users, [
  //     {
  //       email: String
  //     }
  //   ]);

  //   users.forEach(({ email }) => {
  //     const userExists = Accounts.findUserByEmail(email);

  //     if (userExists) {
  //       return true;
  //     } else {
  //       return false;
  //     }
  //   });
  // }
});
