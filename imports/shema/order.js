import { Orders } from "../api/orders";

Orders.schema = new SimpleSchema({
  // id: {
  //   type: String,
  //   regEx: SimpleSchema.RegEx.Id,
  //   unique: true
  // },
  createdAt: {
    type: Date
  },
  detail: {
    type: String
  },
  total: {
    type:Number
  },
  status: {
    type: Number
  },
  buyer_id: {
    type: String
  }
});
