import category from "./category";
import order from "./order";
import product from "./product";
import subCategory from "./category";
import user from "./user";
import lady from "./category";
import productDetail from "./productDetail"

export { category, order, product, subCategory, user, lady, productDetail };
