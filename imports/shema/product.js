import { Products } from "../api/products";

Products.schema = new SimpleSchema({
  name: {
    type: String,
    max: 120
  },
  price: {
    type: Number
  },
  image: {
    type: [String]
  }
});
