import { Ladies } from "../api";

Ladies.schema = new SimpleSchema({
  mainCate: {
    type: String
  },
  subCate: {
    type: String
  },
  productId: {
    type: String
  }
});
