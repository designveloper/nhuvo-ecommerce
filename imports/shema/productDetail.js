import { ProductDetails } from "../api/productDetails";

ProductDetails.schema = new SimpleSchema({
  productId: {
    type: String
  },
  size: {
    type: Object
  },
  quantity: {
    type: Number
  },
  description: {
    type: String
  },
  color: {
    type: Array
  },
  brand: {
    type: String
  },
  inStock: {
    type: Number
  },
  mainCate: {
    type: String
  },
  subCate: {
    type: String
  }
});
